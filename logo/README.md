Our logo is the [https://en.wikipedia.org/wiki/Triskelion Triskelion], a Celtic symbol of evolution and wisdom.  The name we chose ''Trisquel'' is the Spanish word for the triple spiral form.  

''Triskelion'' resembles the [https://upload.wikimedia.org/wikipedia/commons/4/4a/Debian-OpenLogo.svg Debian logo], in recognition of the distribution that our first edition was based on. We now base Trisquel on Ubuntu, itself a Debian derivative.

The base color we use is #004DB1, and the font is Droid Sans.

==Copyright, trademark and license==

'''"Trisquel"''' and the Trisquel logo are registered trademarks owned by Rubén Rodríguez Pérez. Spanish Trademark and patent office, file M 2855910, Niza class 09. Consult the [http://www.oepm.es Spanish Government's Patent Representatives]. 

[http://www.oepm.es/es/signos_distintivos/resultados_expediente.html?mod=M&exp=2855910&bis= Officially registered image. See.]

The logo (without the name) is licensed under the terms of the GNU GPL:

<pre>
    Copyright © 2004 Rubén Rodríguez Pérez
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
</pre>

Due to trademark concerns, the logo including the name is released under the following license:

<pre>
    Copyright © 2004 Rubén Rodríguez Pérez
 
    This logo or a modified version may be used by anyone to refer to the
    Trisquel project, but does not indicate endorsement by the project.
 
    We would appreciate that you make the image a link to
    http://trisquel.info/ if you use it on a web page.
</pre>

